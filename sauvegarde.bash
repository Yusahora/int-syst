#!/bin/bash
source /$home/.backupconf

exec >> $logfile 2>&1
nbSave=$(ls /home/$USER/backup/*.sql | wc -l)

while [ -n "$1" ]; do

    case "$1" in

    -u) read -p "Enter le nom de la base à sauvegarder: "  dbname

        while [ -z "$dbname" ]
        do
          	read -p "Enter le nom de la base à sauvegarder: "  dbname
        done

	echo $dbname
        echo $d

        if [ ! -d /home/$USER/backup ]
        then
            	mkdir /home/$USER/backup
        else
            	mysqldump -u $login -p$pwd $dbname > /home/$USER/backup/$dbname$d.sql

        fi;;
    -m)
                mysqldump -u $login -p$pwd --databases $databases > /home/$USER/backup/multiple-databases$d.sql
        ;;

    -a)         if [ ! -d /home/$USER/backup ]
        then
            	mkdir /home/$USER/backup
        else
            	mysqldump -u $login -p$pwd --all-databases > /home/$USER/backup/all-databases$d.sql

        fi;;

    -h) echo "
              	-u sauvegarde une base de donnée
                -m sauvegarde plusieurs bases de données
                -a sauvegarde toute les bases de données";;

    *) echo "Option $1 non accepté" ;;

    esac

    shift

done

if [[ $nbSave -ge 5 ]]; then
        nbDel=$(expr $nbSave - 5 + 1)
        listDel=$(ls /home/$USER/backup/*.sql | sort | head -$nbDel)
        for save in $listDel
        do
          	rm -rf /home/$USER/backup/$save
        done
fi
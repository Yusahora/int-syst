# Sommaire

* [I. Présentation ](#i-presentation)
  * [1. Zabbix](#2-zabbix)
  * [2. Mattermost](#3-mattermost)
* [II. Installation](#ii-installation)
  * [1. Installation de zabbix](#1-installation-de-zabbix)
  * [2. Installation de mattermost](#2-installation-de-mattermost)
  

# I Présentation

* Pour ce Tp file rouge j'ai choisie de mettre en place un solution de monitoring doublé d'un server de chat pour envoyer des alertes en cas de problèmes signaler par ma solution de monitoring, j'ai donc choisie zabbix comme serveur de monitoring et Mattermost comme server de chat.

## 1. Zabbix

* Zabbix est un logiciel open source créé par Alexei Vladishev en 1998 il permet de surveiller l'état de divers machines et serveurs sur un réseau et de traduire les données reçu en graphique dynamique. La dernière version à ce jour est la 4.2.6 c'est d'ailleur la version utiliser dans ce tp.

## 2. Mattermost

* Mattermost est un service de chat open source dévelloper par Mattermost Inc, ce service est souvent utiliser comme chat interne en entreprise, dans notre cas il nous sert à recevoir des alertes envoyer par zabbix mais il est tout à fait envisageable de s'en servir aussi comme chat dans le cadre d'un entreprise. La dernière version à ce jour est la 5.11.0 c'est d'ailleur la version utiliser dans ce tp.


# II Installation

* Les parties suivantes seront consacrées aux procédures d'installations et de sauvegarde de nos différents services.

## 1. Installation de zabbix

* Les prérequis à l'installation de Zabbix sont :
    1. Un server web Apache
    2. Php (version 5 minimum) et les extensions php requises
    3. Un server MySql/MariaDB
* Cette exemple d'installation se fait sous centos 7 pour le server et ubuntu pour le client
* Plus d'informations sont disponible sur ce site (https://www.zabbix.com/documentation/3.0/manual/installation/requirements)

* L'installtion de zabbix est assez simple pour cela il faut créé un utilisateurs zabbix et une base de donnée zabbix dans votre serveur mysql ou mariadb et donner tout les droit a l'utilisateur zabbix sur la base de donnée zabbix. Ensuite pour installer les package de zabbix ajouter le repo zabbix comme cela: 
```

rpm -ivh https://repo.zabbix.com/zabbix/4.0/rhel/7/x86_64/zabbix-release-<version-voulue>.el7.noarch.rpm

```

* Puis lancer l'installation

```
yum install -y zabbix-server-mysql  zabbix-web-mysql zabbix-agent zabbix-get

```

* un fois cela fait modifier les fichiers suivants comme nécessaire :

```
/etc/httpd/conf.d/zabbix.conf

/etc/zabbix/zabbix_server.conf

```
* plus d'information sur comment modifier les fichiers sont disponible sur ce site(https://www.fosslinux.com/7705/how-to-install-and-configure-zabbix-on-centos-7.htm)

* il ne vous reste plus qu'a effectuer les actions suivantes pour configurer votre server:

```
cd /usr/share/doc/zabbix-server-mysql-4.0.4/  

zcat create.sql.gz | mysql -u zabbixuser -p fosslinuxzabbix  

systemctl restart zabbix-server.service  

systemctl enable zabbix-server.service #cette ligne permet à zabbix de se lancer tout seul au démarage  

```
* les 3 lignes suivantes servent a autoriser les traffic nécessaire au fontionnelent de zabbix

```

firewall-cmd --add-service={http,https} --permanent
firewall-cmd --add-port={10051/tcp,10050/tcp} --permanent
firewall-cmd --reload
systemctl restart httpd

```

* Si tout va bien vous devrier avoir accès à l'interface graphique de zabbix grace à cette url http://Nom-du-server-ou-ip/zabbix/ ce qui vous amène sur cette page:
![alt text](/screens/Zabbix-Welcome-Page.png "open page")
* Appuyer sur next puis vous arriverez sur cette page:
![alt text](/screens/Check-Of-Pre-Requisites.png "check")
* Il ne vous reste plus qu'a suivre les différentes étapes et votre serveur zabbix est prêt les idenfitfiant de connexion de base sont "admin" avec "zabbix" comme mot de passe.

* Pour que zabbix puissent récupérer des informations il est nécessaire d'installer un agent sur les machines que l'on veut monitorer pour cela il suffit de faire les commandes suivantes:  


```
wget https://repo.zabbix.com/zabbix/4.0/ubuntu/pool/main/z/zabbix-release/zabbix-release_<version-voulue>+bionic_all.deb  

dpkg -i zabbix-release_<version-voulue>+bionic_all.deb  

apt update

apt-get install -y zabbix-agent

```
* puis il suffit de rentrer les infromations de votre serveur dans le fichier `` /etc/zabbix/zabbix_agentd.conf `` puis de redémarrer le zabbiz agent.

* Il ne vous reste plus qu'a créé un host dans l'interface graphique de zabbix dans configuration>Hosts>Create host. Vous devriez maintenant voir apparaitre les données de votre client dans le graph de zabbix.


## 2. Installation de mattermost

* Pour installer mattermost il suffit de télécharger mattermost avec la commande suivante:

```
wget https://releases.mattermost.com/<version-voulue>/mattermost-<version-voulue>-linux-amd64.tar.gz

```
* puis d'extraire les fichiers et de les déplacer dans /opt


```

tar -xvzf *.gz
sudo mv mattermost /opt
* ensuite créé le dossier data et le fichier mattermost.service


mkdir /opt/mattermost/data
touch /etc/systemd/system/mattermost.service
* faite les changement nécessaire dans


/etc/systemd/system/mattermost.service
/opt/mattermost/config/config.json
* Des examples sont trouvable sur le site de mattermost (https://docs.mattermost.com/install/install-rhel-7.html)
* créé un utilisateurs mattermost et donner lui les droits nécessaire


useradd --system --user-group mattermost
chown -R mattermost:mattermost /opt/mattermost
chmod -R g+w /opt/mattermost
chmod 664 /etc/systemd/system/mattermost.service
* pour finir reload les services et démarrer mattermost


systemctl daemon-reload
systemctl enable mattermost
systemctl start mattermost


* il ne vous rest plus qu'a accéder à l'interace web de mattermost grace à l'url suivant http://Nom-du-server-ou-ip:8065.
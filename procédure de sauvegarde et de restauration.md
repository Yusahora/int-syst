# Exemple de procédure de sauvegarde et de restauration pour un macbook pro

## Sauvegarde

* Matériel nécessaire: un disque dur de taille supérieur ou égal à la taille de l'ordinateur ou une time capsule

* Pour commencer, brancher le  disque dur ou la time capsule au macbook, ensuite si c'est un disque dur le macbook vous demande si vous voulez utiliser ce disque dur comme time machine cliquer sur oui.
* Ensuite aller dans les préférences système puis time machine puis cliquer sur sauvegarder.

## Restauration

* Au moment du démarage de l'ordinateur maintener enfoncer les touches cmd+r, vous vous retrouvez sur l'interface de restauration du macbook cliquez sur restaurer depuis une sauvegarde depuis une time machine selectionner la sauvegarde voule et attendez la fin de la restauration.
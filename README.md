# int-syst

## Ensemble des documents créé dans le cadre du cours d'intégration système

*[procédure de sauvegarde et de restauration](https://gitlab.com/Yusahora/int-syst/blob/master/proc%C3%A9dure%20de%20sauvegarde%20et%20de%20restauration.md)

*[infrastructure proposée](https://gitlab.com/Yusahora/int-syst/blob/master/infrastructure%20propos%C3%A9e.md)

*[Script bash](https://gitlab.com/Yusahora/int-syst/blob/master/sauvegarde.bash)
